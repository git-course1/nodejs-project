const login = (username, password) => {
  if (username == "memam" && password == "123")
    return `User: ${username} logged in successfully`;
  else return `User: ${username} is not found`;
};

const fbLogin = (username, password) => {
  if (username == "memam" && password == "123")
    return `User: ${username} logged in successfully using Facebook`;
  else return `User: ${username} is not found`;
};

const logout = (username) => {
  return `User: ${username} logged out successfully`;
};

const sigup = (email, password) => {
  return `User: ${email} registered successfully, please verify your email`;
};

module.exports = {
  login,
  fbLogin,
  logout,
  sigup,
};
