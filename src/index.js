const express = require("express");

const auth = require("./authentication");
const cart = require("./cart");

const app = express();

app.get("/", (req, resp) => resp.send("Hello World!"));

app.get("/login", (req, resp) => {
  const username = req.query.user;
  const password = req.query.password;

  if (username == undefined || password == undefined)
    resp.send("please send user and password.");
  else resp.send(auth.login(username, password));
});

app.get("/fblogin", (req, resp) => {
  const username = req.query.user;
  const password = req.query.password;

  if (username == undefined || password == undefined)
    resp.send("please send user and password");
  else resp.send(auth.fbLogin(username, password));
});

app.get("/logout", (req, resp) => {
  const username = req.query.user;

  if (username == undefined) resp.send("please send user and password");
  else resp.send(auth.logout(username));
});

app.get("/addToCart", (req, resp) => {
  const itemId = req.query.itemId;
  const count = req.query.count;

  if (itemId == undefined || count == undefined)
    resp.send("please enter a vaild itemId and count");
  else resp.send(cart.addToCart(itemId, count));
});

app.get("/signup", (req, resp) => {
  const email = req.query.email;
  const password = req.query.password;

  if (email == undefined || password == undefined)
    resp.send("please send email and password");
  else resp.send(auth.sigup(email, password));
});

app.listen(3000, () => console.log("API server is started"));
