const addToCart = (itemId, count) => {
  return `${count} item(s) of ${itemId} added`;
};

module.exports = {
  addToCart,
};
